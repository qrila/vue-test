var eventBus = new Vue()

Vue.component('product', {
  props: {
    premium: {
      type: Boolean,
      required: true
    },
    cart: {
      type: Array,
      required: true
    }
  },
  template: `
    <div class="product">
      <div class="product-image">
        <img :src="image" :alt="altText">
      </div>

      <div class="product-info">
          <h1>{{ title }}</h1>

          <p v-if="inStock">In Stock</p>
          <p v-else>Out of Stock</p>
          <p>Shipping: {{ shipping }}</p>
          
          <product-details :details="details" />

          <div v-for="(variant, index) in variants"
                :key="variant.variantId"
                class="color-box"
                :style="{ backgroundColor: variant.variantColor }"
                @mouseover="updateProduct(index)" >
          </div>

          <button v-on:click="addToCart"
                  :disabled="!inStock"
                  :class="{ disabledButton: !inStock }">Add to cart</button>
          <button v-on:click="decrementCart">-1</button>
      </div>
      <product-tabs :reviews="reviews" />
    </div>
  `,
  data() {
    return {
      brand: 'Vue Mastery',
      product: 'Socks',
      selectedVariant: 0,
      altText: 'A pair of socks',
      onSale: false,
      details: [
        '80% Cotton',
        '20% Polyester',
        'Gender-neutral'
      ],
      variants: [
        {
          variantId: 2234,
          variantColor: 'green',
          variantImage: './assets/vmSocks-green.png',
          variantQuantity: 20
        },
        {
          variantId: 2235,
          variantColor: 'blue',
          variantImage: './assets/vmSocks-blue.png',
          variantQuantity: 5
        }
      ],
      reviews: []
    }
  },
  methods: {
    addToCart() {
      this.$emit('update-cart', 'add', this.variants[this.selectedVariant])        
    },
    decrementCart() {
      this.$emit('update-cart', 'remove', this.variants[this.selectedVariant])
    },
    updateProduct(index) {
      this.selectedVariant = index
    }
  },
  computed: {
    title() {
      return this.brand + ' ' + this.product + (this.onSale ? ', Item on SALE' : '')
    },
    image() {
      return this.variants[this.selectedVariant].variantImage
    },
    inStock() {
      return this.variants[this.selectedVariant].variantQuantity
    },
    shipping() {
      if(this.premium) {
        return "Free"
      }
      return "2.99"
    }
  },
  mounted() {
    eventBus.$on('review-submitted', productReview => {
      this.reviews.push(productReview)
    })
  }
})

Vue.component('product-details', {
  props: {
    details: {
      type: Array,
      required: true
    }
  },
  template: `
    <ul>
      <li v-for="detail in details">{{ detail }}</li>
    </ul>
  `
})

Vue.component('product-tabs', {
  props: {
    reviews: {
      type: Array,
      required: false
    }
  },
  template: `
    <div>
      <div>
        <span class="tab"
              :class="{ activeTab: selectedTab === tab }"
              v-for="(tab, index) in tabs" :key="index"
              @click="selectedTab = tab"
        >{{ tab }}</span>
      </div>

      <div v-show="selectedTab === 'Reviews'">
        <p v-if="!reviews.length">There are no reviews</p>
        <ul v-else>
          <li v-for="(review, index) in reviews" :key="index">
            <p>{{ review.name }}</p>
            <p>Rating: {{ review.rating }}</p>
            <p>{{ review.review }}</p>
            <p>Endorsement: {{ review.endorsement }}</p>
          </li>
        </ul>
      </div>

      <div v-show="selectedTab === 'Make a review'">
        <product-review />
      </div>
    </div>
  `,
  data() {
    return {
      tabs: ['Reviews', 'Make a review'],
      selectedTab: 'Reviews'
    }
  }
})

Vue.component('product-review', {
  template: `
    <div>
      <p v-if="errors.length">
        <b>Please correct the following error(s):</b>
        <ul>
          <li v-for="error in errors">{{ error }}</li>
        </ul>
      </p>
      <form class="review-form" @submit.prevent="onSubmit">
        <p>
          <label for="name">Name:</label>
          <input id="name" v-model="name" placeholder="name">
        </p>

        <p>
          <label for="review">Review:</label>
          <textarea id="review" v-model="review" placeholder="review"></textarea>
        </p>
        <p>
          <label for="rating">Rating:</label>
          <select id="rating" v-model.number="rating">
            <option>5</option>
            <option>4</option>
            <option>3</option>
            <option>2</option>
            <option>1</option>
          </select>
        </p>

        <div>
          <label for="endorsement">Do you want to endorse the product</label>
          <div id="endorsement">
            <input type="radio" id="yesendorsement" value="Yes" v-model="endorsement">
            <span>Yes</span>
            <input type="radio" id="noendorsement" value="No" v-model="endorsement">
            <span>No</span>
          </div>
        </div>

        <p>
          <input type="submit" value="Submit">
        </p>
      </form>
    </div>
  `,
  data() {
    return {
      name: null,
      review: null,
      rating: null,
      endorsement: null,
      errors: []
    }
  },
  methods: {
    onSubmit() {
      if (this.name && this.review && this.rating && this.endorsement) {
        let productReview = {
          name: this.name,
          review: this.review,
          rating: Number(this.rating),
          endorsement: this.endorsement
        }
        eventBus.$emit('review-submitted', productReview)
        this.name = null
        this.review = null
        this.rating = null
        this.endorsement = null
        this.errors = []
      } else {
        this.errors = []
        if (!this.name) this.errors.push("Name required.")
        if (!this.review) this.errors.push("Review required.")
        if (!this.rating) this.errors.push("Rating required.")
        if (!this.endorsement) this.errors.push("Endorsement required.")
      }
    }
  }
})

var app = new Vue({
  el: '#app',
  data: {
    premium: true,
    cart: []
  },
  methods: {
    updateCart(action, item) {
      if (action === 'add' && this.cart.reduce((acc,cur) => {
        if (cur.variantId === item.variantId) {
          acc += 1
        }
        return acc
      },0) < item.variantQuantity) {
        this.cart.push(item)
      } else if (action === 'remove') {
        const foundItem = this.cart.indexOf(item)
        if (foundItem > -1) {
          this.cart.splice(foundItem, 1)
        }
      }
    }
  }
});